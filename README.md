# Infinichat: A simple chat program.
A Websockets test. Client in JavaScript and server in Python using the `websockets` library. 

# Protocol
Each message is a JSON object with the field `type` defining the message type. Other fields depend on message type.
## `userJoin`
Sent by client after connecting to the server and sent by server to notify other clients.
### Fields
* `user` The user's desired nickname.
## `userLeave`
Sent by server when a user disconnects.
## `sendMsg`
Contains a chat message, sent by both client and server.
### Fields
* `text` The text of the message.
* `user` The nickname of the user who sent the message. (Ignored by server if provided by client)
## `error`
Sent by server to report an error to the client.
### Fields
* `errorType` The error that occured.
### Errors
* `nickTaken` The user's requested nickname is taken.