const chatLog = document.getElementById("chatlog");
const chatBox = document.getElementById("chat");
const SERVER = "ws://" + window.location.hostname + ":8001";
var nickname = "foobar";
var hasConnected = false;
var socket;

function onPageLoad(evt) {
    nickname = prompt("Enter a nickname:");
    socket = new WebSocket(SERVER, "x-infichat");
    socket.addEventListener("open", onSocketOpen);
    socket.addEventListener("message", onSocketMsg);
    socket.addEventListener("error", onSocketError);
    socket.addEventListener("close", onSocketClose);
}

function onPageUnload(evt) {
    // Keeping just in case...
}

function createLogText(str) {
    // I have no clue what these mean ethier. It's regex, no one with a life understands it.
    const BOLD = /\*\*(.*)\*\*/gim
    const ITALIC = /\*(.*)\*/gim
    const STRIKE = /\~\~(.*)\~\~/gim

    var escaper = document.createElement("p"); // Only good way I could find to do this.
    escaper.textContent = str;
    str = escaper.innerHTML;
    escaper.remove();

    var newText = document.createElement("span");
    newText.innerHTML = str.replace(BOLD, "<strong>$1</strong>")
                           .replace(ITALIC, "<em>$1</em>")
                           .replace(STRIKE, "<s>$1</s>");
    return newText;
}

function logChat(user, msg) {
    var newLine = document.createElement("p");
    var newName = document.createElement("b");
    var newText = createLogText(msg);
    
    newText.className = "message";
    newName.className = "username";
    newName.textContent = user;

    newLine.appendChild(newName);
    newLine.appendChild(newText);
    chatLog.appendChild(newLine);
    newLine.scrollIntoView();
}

function onSocketOpen(evt) {
    var obj = {type: "userJoin", user: nickname};
    socket.send(JSON.stringify(obj));
    hasConnected = true;
}

function onSocketMsg(evt) {
    var obj = JSON.parse(evt.data);
    switch (obj.type) {
        case "sendMsg":
            logChat(obj.user, obj.text);
            break;
        case "error":
            handleChatError(obj.errorType);
            break;
    }
}

function onSocketError(evt) {
    console.error(evt);
}

function onSocketClose(evt) {
    console.info(evt);
    if (hasConnected)
        alert(`The connection to the server has been lost.\nCode: ${evt.code}`);
    else
        alert(`Unable to connect to server.\nCode: ${evt.code}`);
}

function handleChatError(err) {
    switch (err) {
        case "nickTaken":
            alert("Sorry, that name is taken.");
            window.location.reload();
            break;
    }
}

function onSubmitText(evt) {
    var formData = new FormData(evt.target);
    var msg = {type: "sendMsg", text: formData.get("text")};
    socket.send(JSON.stringify(msg));
    evt.preventDefault();
    chatBox.reset();
}

window.addEventListener("load", onPageLoad);
window.addEventListener("beforeunload", onPageUnload);
chatBox.addEventListener("submit", onSubmitText);