import asyncio, websockets, json

# import logging
# logger = logging.getLogger('websockets')
# logger.setLevel(logging.DEBUG)
# logger.addHandler(logging.StreamHandler())

users = {}

async def handler(websocket):
    # While the user is connected
    async for rawmsg in websocket:
        message = json.loads(rawmsg)
        match message["type"]:
            case "userJoin":
                nick = message["user"]
                if not nick in users: # If the requested nickname is not taken.
                    users[nick] = websocket
                    send_msg(message, websocket)
                    print(nick, "joined")
                else:
                    mymsg = {"type": "error", "errorType": "nickTaken"}
                    await websocket.send(json.dumps(mymsg))
            case "sendMsg":
                send_msg(message, websocket)
                print(message["user"], "says", message["text"])
            case _:
                print("Unknown message:", message)
    # After disconnection
    send_leave_msg(websocket)
    nick = get_nick_from_socket(websocket)
    del users[nick]
    print(nick, "left")

def get_nick_from_socket(target_sock):
    # Stop nickname spoofing.
    for nick, sock in users.items():
        if sock == target_sock:
            return nick

def send_msg(message, sendsock):
    recipients = set() # websockets.broadcast() only takes a set as first param.
    message["user"] = get_nick_from_socket(sendsock)
    for nick in users.keys():
        recipients.add(users[nick])
    websockets.broadcast(recipients, json.dumps(message))

def send_leave_msg(leaving_sock):
    mymsg = {"type": "userLeave"}
    send_msg(mymsg, leaving_sock)

async def main():
    async with websockets.serve(handler, "", 8001, subprotocols=["x-infichat"]):
        await asyncio.Future()  # run forever

if __name__ == "__main__":
    asyncio.run(main())